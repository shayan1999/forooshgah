import React ,{Component} from 'react'
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    FlatList,
    StyleSheet,
    Dimensions,
    ScrollView
} from 'react-native';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");


const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);

export default class Agahi extends Component{
    constructor(props){
        super(props)
        this.state={
            run:true,
            flat:[],
            flat2:[]
        }
    }
    static navigationOptions = {
        title:'محصول',
        headerStyle: {
          backgroundColor: '#0a1924',
        },
        headerTitleStyle: {
            color:'white',
            marginLeft:widthh*0.6,
            fontSize:18
        },
        headerTintColor: 'white'
      };
    go(name){
        this.setState({modalVisible2: false})
        var Forooshgah = Parse.Object.extend("Forooshgah");
        var query = new Parse.Query(Forooshgah);
        query.equalTo('name', name)
        query.find().then(results => {
          this.props.navigation.navigate('Forooshgah',{
            name: results[0].get('name'),
            image: results[0].get('Link'),
            address: results[0].get('address'),
            phone: results[0].get('phone')
          })
        });
      }

    run(a,b,c){
        //alert(a.length)
        var flat=[]
        for (i=0; i<a.length; i++) {
            flat.push({
                'name':a[i],
                'detail':b[i]
            })
        }
        var flat2=[]
        for (i=0; i<c.length; i++) {
            flat2.push({
                'link':c[i]
            })
        }
        //alert(flat[0].detail)
        this.setState({flat: flat, flat2:flat2})
    }

    render(){

        const { navigation } = this.props;
        if(this.state.run){
            //alert(navigation.getParam('jadvalName').length)
            this.run(navigation.getParam('jadvalName'),navigation.getParam('jadvalTozih'), navigation.getParam('pics'))
            this.setState({run:false})
        }
        
        return(
            <View style={{width:'100%', height:'100%', justifyContent:'center', alignItems:'center', backgroundColor:'rgb(240,240,240)'}}>
                <ScrollView>
                    <View style={{height:3*heightt/7, width:widthh*0.95, backgroundColor:'white', borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                        <FlatList 
                            data={this.state.flat2}
                            horizontal={true}
                            renderItem={({item , index})=> {  
                                return(
                                    <Image style={{height:(3*heightt/7)-20,width:widthh, alignSelf:'center', borderRadius:10}} source={{uri: item.link}}/>
                                );  
                                }} 
                        />     
                    </View>
                    <View style={{display:'flex', flexDirection:'row',justifyContent:'flex-start', alignItems:'center'}}>
                        <View style={{flex:1}}>

                        </View>
                        <Text style={{flex:7, color:'black', fontSize:24, borderWidth:1, textAlign:'center', width:'100%', marginTop:10, padding:10, borderColor:'dodgerblue', backgroundColor:'white', borderRadius:10}}>
                            {navigation.getParam('name')}
                        </Text>
                        <View style={{flex:1}}>

                        </View>
                    </View>
                    <View style={{marginLeft:20, marginRight:20,marginTop:10 ,display:'flex', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}}> 
                            <TouchableOpacity style={{flex:1, backgroundColor:'dodgerblue', borderRadius:100}} onPress={()=> this.go(navigation.getParam('forooshgah'))}>
                                <Text style={{textAlign:'center', fontSize:widthh*0.04, padding:5, color:'white'}}> 
                                    بزن بریم
                                </Text>
                            </TouchableOpacity>
                            <Text style={{flex:1, textAlign:'right', fontSize:widthh*0.05}}>
                                فروشگاه {navigation.getParam('forooshgah')}
                            </Text>
                        </View>
                    <View style={{display:'flex', flexDirection:'column', justifyContent:'flex-start', alignItems:'center', marginTop:30}}>
                        <View style={{display:'flex', justifyContent:'center', alignItems:'center' ,height:heightt*0.1, width:widthh*0.9,borderWidth:2, borderColor:'dodgerblue', backgroundColor:'white', borderTopRightRadius:10, borderTopLeftRadius:10}}>
                            <Text style={{fontSize:widthh*0.045, color:'black'}}>
                                ویژگی های محصول
                            </Text>
                        </View>
                        <View style={{width:0.9*widthh, display:'flex', flexDirection:'row', justifyContent:'flex-start', alignItems:'center', borderBottomWidth:2, borderLeftWidth:2, borderRightWidth:2, borderColor:'dodgerblue'}}>
                                    <View style={{flex:2}}>
                                        <Text style={{fontSize: 14, textAlign:'center', paddingTop:10}}>
                                            {navigation.getParam('gheymat')}
                                        </Text>  
                                    </View>
                                    <View style={{flex:1, display:'flex', justifyContent:'center', alignItems:'center', borderLeftWidth:2, borderColor:'dodgerblue'}}>
                                        <Text style={{fontSize: 20, textAlign:'right', paddingTop:10}}>
                                            قیمت
                                        </Text>
                                    </View>
                                </View>
                        <View style={{marginBottom:20}}>
                        <FlatList
                            data={this.state.flat}
                            horizontal={false}
                            // key={counter%2}
                            renderItem={({item , index})=> {  
                            return(
                                <View style={{width:0.9*widthh, display:'flex', flexDirection:'row', justifyContent:'flex-start', alignItems:'center', borderBottomWidth:2, borderLeftWidth:2, borderRightWidth:2, borderColor:'dodgerblue'}}>
                                    <View style={{flex:2}}>
                                        <Text style={{fontSize: 14, textAlign:'center', paddingTop:10}}>
                                            {item.detail}
                                        </Text>  
                                    </View>
                                    <View style={{flex:1, display:'flex', justifyContent:'center', alignItems:'center', borderLeftWidth:2, borderColor:'dodgerblue'}}>
                                        <Text style={{fontSize: 20, textAlign:'right', paddingTop:10}}>
                                            {item.name}
                                        </Text>
                                    </View>
                                </View>
                            );  
                            }}                    
                        >

                        </FlatList>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}