import React, { Fragment, Component } from 'react';
import {AsyncStorage} from 'react-native';

import {
    View,
    TouchableOpacity,
    Image,
    Text,
    FlatList,
    StyleSheet,
    Dimensions
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay'
import Parse from 'parse/react-native';
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");



const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);


class FlatListItem extends Component{
    constructor(props){
      super(props)
      this.state = {
        activeRowKey: null,
      };
    }

    render(){
        
        return(
            <View style={{width:'100%', height:heightt*0.25 , backgroundColor:'white', borderBottomColor:'black', borderWidth:1}}>
                <TouchableOpacity style={{width:'100%', height:'100%', display:'flex', flexDirection:'row', backgroundColor:'rgb(255,255,255)'}} onPress={() => this.props.parentFlatList.agahi(
                    this.props.item.name, this.props.item.kind, this.props.item.imageUrl, this.props.item.gheymat, this.props.item.takhfif, this.props.item.tozih, this.props.item.forooshgah, this.props.item.jadvalName, this.props.item.jadvalTozih, this.props.item.pics
                )}>
                    <View style={{flex:1, paddingRight:'10%', paddingTop:'2%', paddingBottom:'10%'}}>
                        <View style={{width:'100%', height:'100%', display:'flex', flexDirection:'column', marginLeft:10}}>
                            <Image source={{ uri: this.props.item.imageUrl }} style={{width:heightt*0.2, height:heightt*0.2, borderRadius:20, flex:4, marginBottom:10}} />
                            <Text style={{textAlign:'center', fontSize:14, color:'gray', marginRight:'10%', borderWidth:1, borderColor:'black', borderRadius:5, flex:1, marginBottom:10}}>
                                فروشگاه {this.props.item.forooshgah}
                            </Text>
                        </View>
                    </View>
                    <View style={{flex:1}}>
                        <View style={{width:'100%', height:'100%', display:'flex', flexDirection:'column'}}>
                            <Text style={{flex:3, textAlign:'right', fontSize:16, color:'black', marginRight:'20%', marginTop:'25%', borderBottomColor:'dodgerblue', borderBottomWidth:1}}>
                                {this.props.item.name}   
                            </Text>
                            <Text style={{flex:1, textAlign:'right', fontSize:14, color:'green', marginRight:'20%'}}>
                                {String(this.props.item.gheymat)} تومان
                            </Text>
                            {this.props.item.takhfif !== "No" &&
                                <Text style={{borderTopColor:'dodgerblue', borderTopWidth:1, flex:1, textAlign:'right', fontSize:14, color:'red', marginRight:'20%', textDecorationLine: 'line-through', textDecorationStyle: 'solid'}}>
                                    {String(this.props.item.takhfif)} تومان
                                </Text>
                            } 
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
  }

export default class Finder extends Component{
    static navigationOptions = {
        title:'دسته',
        headerStyle: {
          backgroundColor: '#0a1924',
        },
        headerTitleStyle: {
            color:'white',
            marginLeft:widthh*0.65
        },
        headerTintColor: 'white'   
    };

    constructor(props){
        super(props)
            this.state={
                run:true,
                spinner:true
            }
    }

     async componentDidMount(){
        var Agahi = Parse.Object.extend("Agahi");
        var query = new Parse.Query(Agahi);
        var flatListData = [];
        // Prefer arrow functions here
        //alert(daste)
        query.equalTo('kind', this.props.navigation.getParam('daste'))
        query.ascending("Forooshgah");
        await query.find().then(results => {
            // Works like your classic for(…) loop, but more efficient
            for (const result of results) {
                // flatListData.push(String(result.get("taklifName")));
                // alert(String(result.get("username")))
                // alert(String(result.get("Name")))
                flatListData.push({
                    key: result.id,
                    imageUrl: String(result.get("Link")),
                    name: String(result.get("Name")),
                    gheymat: String(result.get("gheymat")),
                    forooshgah: String(result.get("Forooshgah")),
                    takhfif: String(result.get("takhfif")),
                    kind: String(result.get("kind")),
                    tozih: String(result.get('detail')),
                    jadvalName: result.get('jadvalName'),
                    jadvalTozih: result.get('jadvalTozih'),
                    pics: result.get('pics')
                });
            }
            // Resolve your function and send the result back
            //this.setState({flat : flatListData, spinner:false})

        }, error => {
            //alert(error.message)
        });
        //const myVariable = await getFlatListData();
        this.setState({flat : flatListData, spinner:false})
    }

    agahi(name, kind, image, gheymat, takhfif, tozih, forooshgah, jn, jt, pics){
        this.props.navigation.navigate('Agahi',{
            name: name,
            kind: kind,
            image: image,
            gheymat: gheymat,
            takhfif: takhfif,
            tozih: tozih,
            forooshgah: forooshgah,
            jadvalName: jn,
            jadvalTozih: jt,
            pics: pics
        })
        
    }

    about(){
        this.props.navigation.dispatch({
            type: 'ReplaceCur',
            routeName: 'Info',
            key: 'Infoo'
          })
    }

    render(){
        return(
            <View style={{ flex: 1, display:'flex', flexDirection:'column', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: 'white'}}>
                <View style={{flex: 11, backgroundColor:'rgb(245,245,245)', width:'100%'}}>
                    <Spinner 
                      visible ={this.state.spinner}
                      textContent={'لطفا صبر کنید...'}
                      textStyel={{color:'#24305E'}}
                      color='#24305E'
                    />
                    <FlatList
                        data={this.state.flat}
                        renderItem={({item , index})=> {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                        // console.log(Item = ${item} , Index = ${index})
                        return(<FlatListItem item = {item} index = {index} parentFlatList={this} >  

                        </FlatListItem>);  //FlatListItem props : item , index
                        }}                    //When State changes, automatically re-render components
                    >

                    </FlatList>
                </View>
            </View>
        )
    }
}