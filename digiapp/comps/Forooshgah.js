import React, { Fragment, Component } from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    ScrollView
} from 'react-native';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");

const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);

class FlatListItem extends Component{
    constructor(props){
        super(props)
        this.state = {
        activeRowKey: null,
        };
    }

    render(){
        return(
            <View style={{width:widthh*0.46, height:heightt*0.3, marginRight:widthh*0.03, borderWidth:2, borderColor:'black', borderRadius:5, display:'flex', flexDirection:'column', marginTop:heightt*0.01, backgroundColor:'white'}}>
            <TouchableOpacity style={{width:'100%', height:'95%',borderRadius:widthh/12, marginTop:'5%', display:'flex', flexDirection:'column'}} onPress={() => this.props.parentFlatList.agahi(
                        this.props.item.name, this.props.item.kind, this.props.item.imageUrl, this.props.item.gheymat, this.props.item.takhfif, this.props.item.tozih, this.props.item.forooshgah, this.props.item.jadvalName, this.props.item.jadvalTozih, this.props.item.pics)}>  
                <Image source={{ uri: this.props.item.imageUrl }} style={{flex:1, height:'100%', width:'60%', alignSelf:'center', marginTop:'5%'}} />
                <View style={{flex:1, display:'flex', marginTop:'5%'}}>
                <Text style={{flex:1, textAlign:'right', paddingRight:'5%'}}>
                    {this.props.item.tozih.slice(0,20)}...
                </Text>
                <Text style={{flex:1, color:'red', textAlign:'right', paddingRight:'8%'}}>
                    {this.props.item.gheymat} تومان
                </Text>
                {this.props.item.takhfif != 'No' &&
                    <Text style={{flex:1, color:'dodgerblue', textAlign:'right', paddingRight:'8%'}}>
                        {this.props.item.takhfif} تومان
                    </Text>
                }
                </View>
            </TouchableOpacity>
            </View>
        )
    }
}

export default class Info extends Component{
    static navigationOptions = {
        title:'فروشگاه',
        headerStyle: {
          backgroundColor: '#0a1924',
        },
        headerTitleStyle: {
            color:'white',
            marginLeft:widthh*0.6,
            fontSize:18
        },
        headerTintColor: 'white'
      };
      constructor(props){
        super(props)
            this.state={
                run: true,
                flat: []
            }
      }
      
    async componentDidMount(){
        var flatListData = [];
        var Agahi = Parse.Object.extend("Agahi");
        var query = new Parse.Query(Agahi);
        // Prefer arrow functions here
        //alert(this.props.navigation.getParam('name'))
        query.equalTo('Forooshgah', this.props.navigation.getParam('name'))
        query.ascending("kind");
        await query.find().then(results => {
            // Works like your classic for(…) loop, but more efficient
            for (const result of results) {
                // flatListData.push(String(result.get("taklifName")));
                // alert(String(result.get("username")))
                // alert(String(result.get("Name")))
                flatListData.push({
                    "key": result.id,
                    "imageUrl": String(result.get("Link")),
                    "name": String(result.get("Name")),
                    "gheymat": String(result.get("gheymat")),
                    "forooshgah": String(result.get("Forooshgah")),
                    "takhfif": String(result.get("takhfif")),
                    "kind": String(result.get("kind")),
                    "tozih": String(result.get('detail')),
                    "jadvalName": result.get('jadvalName'),
                    "jadvalTozih": result.get('jadvalTozih'),
                    "pics": result.get('pics')
                });
            }
            // Resolve your function and send the result back

        }, error => {
            //alert(error.message)
        });
        this.setState({flat: flatListData})
    }
    agahi(name, kind, image, gheymat, takhfif, tozih, forooshgah, jn, jt, pics){
        this.setState({modalVisible2: false})
        //alert(jn)
        this.props.navigation.navigate('Agahi',{
            name: name,
            kind: kind,
            image: image,
            gheymat: gheymat,
            takhfif: takhfif,
            tozih: tozih,
            forooshgah: forooshgah,
            jadvalName: jn,
            jadvalTozih: jt,
            pics: pics
        })
      }
    render(){
        return(
          <View style={{ flex: 1, display:'flex', flexDirection:'column', justifyContent: 'center', alignItems: 'center', backgroundColor:'rgb(240,240,240)'}}>
            <ScrollView>
                    <View style={{height:3*heightt/7, width:widthh*0.95, marginLeft:widthh*0.025 , backgroundColor:'white', borderRadius:10, justifyContent:'center', alignItems:'center'}}>
                        <Image style={{height:(3*heightt/7)-20,width:widthh*0.9, alignSelf:'center', borderRadius:10}} source={{uri: this.props.navigation.getParam('image')}}/>
                    </View>
                    <View style={{display:'flex', flexDirection:'column',justifyContent:'flex-start', alignItems:'flex-end'}}>
                        <View style={{display:'flex', flexDirection:'row',justifyContent:'flex-start', alignItems:'center'}}>
                            <View style={{flex:1}}>

                            </View>
                            <Text style={{flex:7, color:'black', fontSize:24, borderWidth:2, borderColor:'dodgerblue' ,textAlign:'center', width:'100%', marginTop:10, padding:10, borderColor:'dodgerblue', backgroundColor:'white', borderRadius:10}}>
                                {this.props.navigation.getParam('name')}
                            </Text>
                            <View style={{flex:1}}>

                            </View>
                        </View>
                        {this.props.navigation.getParam('phone')=='No' &&
                            <Text style={{color:'black', marginRight:10, marginBottom:10, backgroundColor:'white', fontSize:16, textAlign:'right', width:widthh, marginTop:10, padding:10}}>
                                {this.props.navigation.getParam('address')}
                            </Text>
                        }
                        {this.props.navigation.getParam('phone')!='No' &&
                            <Text style={{color:'black', marginRight:10, marginBottom:10, backgroundColor:'white', fontSize:16, textAlign:'right', width:widthh, marginTop:10, padding:10}}>
                                {this.props.navigation.getParam('address')}
                            </Text>
                        }
                        {this.props.navigation.getParam('phone')!='No' &&
                            <TouchableOpacity onPress={()=>Linking.openURL(`tel:${this.props.navigation.getParam('phone')}`)} style={{height:heightt*0.05, backgroundColor:'dodgerblue', width:widthh*0.8, alignSelf:'center', borderRadius:10, display:'flex', flexDirection:'column', justifyContent:'center'}}>
                                <Text style={{textAlign:'center', color:'white'}}>
                                    زنگ بزنیم
                                </Text>
                            </TouchableOpacity>
                        }
                    </View>
                    <View style={{display:'flex', flexDirection:'column', justifyContent:'flex-start', alignItems:'center', marginTop:30}}>     
                        <View style={{marginBottom:20}}>
                        <FlatList
                            data={this.state.flat}
                            horizontal={false}
                            numColumns={2}
                            // key={counter%2}
                            renderItem={({item , index})=> {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                            // console.log(Item = ${item} , Index = ${index})
                            // counter=counter+1
                            return(
                            <FlatListItem item = {item} index = {index} parentFlatList={this} >  

                            </FlatListItem>);  //FlatListItem props : item , index
                            }}                    //When State changes, automatically re-render components
                        >

                        </FlatList>
                        </View>
                    </View>
                </ScrollView>
          </View>
        )
    }
}