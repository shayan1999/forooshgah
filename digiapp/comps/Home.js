import React, { Fragment, Component } from 'react';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");
import Spinner from 'react-native-loading-spinner-overlay'
import {
  View,
  TouchableOpacity,
  Alert,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  Modal,
  FlatList,
  ScrollView,
  TextInput
} from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import Menu from '../images/menu.png'

const getFlatListData = require('../Data/FlatListData2')
const getFlatListDataa = require('../Data/FlatListData3')
const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);

class FlatListItem extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeRowKey: null,
    };
  }

  render(){
      return(
        <View style={{width:widthh*0.47, height:heightt*0.32, marginLeft:widthh*0.01, marginRight:widthh*0.01, borderWidth:2, borderColor:'black', borderRadius:5, display:'flex', flexDirection:'column', marginTop:heightt*0.005, marginBottom:heightt*0.005, backgroundColor: this.props.item.color }}>
          <TouchableOpacity style={{width:'100%', height:'95%',borderRadius:widthh/12, marginTop:'5%', display:'flex', flexDirection:'column'}} onPress={() => this.props.parentFlatList.agahi(
                    this.props.item.name, this.props.item.kind, this.props.item.imageUrl, this.props.item.gheymat, this.props.item.takhfif, this.props.item.tozih, this.props.item.forooshgah, this.props.item.jadvalName, this.props.item.jadvalTozih, this.props.item.pics)}>  
            <View style={{flex:1}}>
              <Image source={{ uri: this.props.item.imageUrl }} style={{height:'100%', width:'70%', alignSelf:'center', marginTop:'1%', borderRadius:8}} />
              <Text style={{position:'absolute', bottom:0, left:'15%', fontSize:16, color:'white', textAlign:'center', borderRadius:100, backgroundColor:'#374991', padding:5}}>
                {this.props.item.darsad}
              </Text>
            </View>
            <View style={{flex:1, display:'flex', marginTop:'5%'}}>
              <Text style={{flex:1, textAlign:'right', paddingRight:'5%', borderBottomWidth:1, borderBottomColor:'lightgray', paddingRight:3, paddingLeft:3}}>
                {this.props.item.tozih.slice(0,20)}...
              </Text>
              <Text style={{flex:1, color:'red', textAlign:'right', paddingRight:'8%', textDecorationLine: 'line-through', textDecorationStyle: 'solid', paddingTop:5}}>
                {this.props.item.gheymat} تومان
              </Text>
              <Text style={{flex:1, color:'#374785', textAlign:'right', paddingRight:'8%', paddingBottom:5}}>
                {this.props.item.takhfif} تومان
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      )
  }
}

class FlatListItem2 extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeRowKey: null,
    };
  }

  render(){
      return(
        <View style={{width:widthh, height:'95%', marginTop:5, borderWidth:2, borderColor:'black', display:'flex', flexDirection:'row', backgroundColor:'white', justifyContent:'center', alignItems:'center', borderRadius:5}}>
          <TouchableOpacity style={{width:'100%', height:'95%',borderRadius:widthh/12, display:'flex', flexDirection:'row', justifyContent:'center', alignItems:'center'}} onPress={() => this.props.parentFlatList.agahi(
                    this.props.item.name, this.props.item.kind, this.props.item.imageUrl, this.props.item.gheymat, this.props.item.takhfif, this.props.item.tozih, this.props.item.forooshgah, this.props.item.jadvalName, this.props.item.jadvalTozih, this.props.item.pics)}>  
            <Image source={{ uri: this.props.item.imageUrl }} style={{flex:2, height:'100%', width:'100%', alignSelf:'center', marginLeft:heightt*0.03}} />
            <View style={{flex:5, backgroundColor:'white', marginLeft:widthh*0.05, borderRadius:5, marginRight:widthh*0.03}}>
              <View style={{flex:1 ,display:'flex', marginTop:'5%', flexDirection:'column'}}>
                <Text style={{flex:1, textAlign:'right', paddingRight:'5%', fontSize:18}}>
                  {this.props.item.name}
                </Text>
                <Text style={{flex:1, color:'#374785', textAlign:'right', paddingRight:'8%', fontSize:14}}>
                  {this.props.item.gheymat} تومان
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )
  }
}

class Home extends Component {
  static navigationOptions = {
    header: null,
    title: null,
  };
  constructor(props){
    super(props)
        this.state={
            run:true,
            modalVisible:false,
            modalVisible2:false,
            data:[],
            digi:false,
            behdasht:false,
            bagh:false,
            sakhteman:false,
            mod:false,
            varzesh:false,
            khane:false,
            daroo:false,
            honar:false,
            nozad:false,
            flat2:[],
            flat3:[],
            spinner:true,
            spinner1:false
        }
  }

  agahi(name, kind, image, gheymat, takhfif, tozih, forooshgah, jn, jt, pics){
    this.setState({modalVisible2: false})
    //alert(jn)
    this.props.navigation.navigate('Agahi',{
        name: name,
        kind: kind,
        image: image,
        gheymat: gheymat,
        takhfif: takhfif,
        tozih: tozih,
        forooshgah: forooshgah,
        jadvalName: jn,
        jadvalTozih: jt,
        pics: pics
    })
  }
  async daste(x){
    this.setState({modalVisible:false})
    await AsyncStorage.setItem('daste',x,() => {
      this.props.navigation.navigate('Finder',{
        daste: x,
        type:'kind'
      })
    });
  }
async search(text){
  var Agahi = Parse.Object.extend("Agahi");
  var query = new Parse.Query(Agahi);

  var Forooshgah = Parse.Object.extend("Forooshgah");
  var query2 = new Parse.Query(Forooshgah);
  var flatListData = [];
  // Prefer arrow functions here
  query.fullText('Name', text)
  query.ascending("Forooshgah");
  query.find().then(results => {
      // Works like your classic for(…) loop, but more efficient
      for (const result of results) {
          // flatListData.push(String(result.get("taklifName")));
          // alert(String(result.get("username")))
          // alert(result.get("jadvalName"))
          flatListData.push({
              "key": result.id,
              "imageUrl": String(result.get("Link")),
              "name": String(result.get("Name")),
              "gheymat": String(result.get("gheymat")),
              "forooshgah": String(result.get("Forooshgah")),
              "takhfif": String(result.get("takhfif")),
              "kind": String(result.get("kind")),
              "tozih": String(result.get('detail')),
              "type": "kala",
              "jadvalName": result.get('jadvalName'),
              "jadvalTozih": result.get('jadvalTozih'),
              "pics": result.get('pics')
          });
      }
      // Resolve your function and send the result back
      //resolve(flatListData);
      query2.fullText('name', text)
      query2.find().then(results => {
          // Works like your classic for(…) loop, but more efficient
          for (const result of results) {
              // flatListData.push(String(result.get("taklifName")));
              // alert(String(result.get("username")))
              //alert(String(result.get("Name")))
              flatListData.push({
                  "key": result.id,
                  "name": String(result.get("name")),
                  "type": 'forooshgah'
              });
          }
          //resolve(flatListData);
          this.setState({flat2 : flatListData, spinner1:false})
          // Resolve your function and send the result back
      }, error=>{
          alert(error.message)
      });
  }, error=>{
      alert(error.message)
  });  
}

  onSwipeLeft() {
    this.setState({modalVisible:true});
  }
  onSwipeRight() {
    this.setState({modalVisible:false});
  }
  info(){
    this.setState({modalVisible: false})
    this.props.navigation.navigate('Info')
  }
  next(){
    this.setState({counter: this.state.counter+1})
  }

  go(name){
    this.setState({modalVisible2: false})
    var Forooshgah = Parse.Object.extend("Forooshgah");
    var query = new Parse.Query(Forooshgah);
    query.equalTo('name', name)
    query.find().then(results => {
      this.props.navigation.navigate('Forooshgah',{
        name: results[0].get('name'),
        image: results[0].get('Link'),
        address: results[0].get('address'),
        phone: results[0].get('phone')
      })
    });
  }

  render() {
      if (this.state.run){
        const myAsyncFunction = async() => {
          const myVariable = await getFlatListData();
          await this.setState({flat : myVariable})
          const myVariable2 = await getFlatListDataa();
          await this.setState({flat3: myVariable2, spinner:false})
          //alert(myVariable[2].jadvalName);
        };
        myAsyncFunction();
        this.setState({run:false})
      }
      const config = {
        velocityThreshold: 0.8,
        directionalOffsetThreshold: 40,
        // gestureIsClickThreshold: 10
      };

      return (
        <View style={{ flex: 1,backgroundColor: 'white', height:heightt, width:widthh}}>   
              <View style={{flex: 1, backgroundColor:'#0a1924', borderBottomWidth:1, borderBottomColor:'black', width:'100%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}}>
                  <TouchableOpacity style={{marginLeft:10, flex:1, height:'100%', justifyContent:'center', alignItems:'center'}} onPress={() => this.setState({modalVisible2:true})}>
                    <Image source={require('../images/search2.png')} style={{height:heightt*0.0512, width:heightt*0.0512}}/>
                  </TouchableOpacity>
                  <View style={{flex:3, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{color:'white', fontSize:16, textAlign:'center', }}>
                      فروشگاه
                    </Text>
                  </View>
                  <View style={{flex:1, padding:'3%', borderRightWidth:0.5, display:'flex'}}>
                    <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}} onPress={() => this.setState({modalVisible:!this.state.modalVisible})} >
                      <Image source={Menu} style={{justifyContent:'center', alignItems:'center', width:40, height:35}}/>
                    </TouchableOpacity>
                  </View>
              </View>
              <View style={{flex: 10, backgroundColor: 'white', width:'100%', display:'flex', flexDirection:'column'}}>
                <View style={{flex:3, display:'flex', flexDirection:'column'}}>
                  <View style={{flex:1, display:'flex', flexDirection:'row', paddingTop:5}}>
                    {/* <View style={{flex:1}} /> */}
                    <Text style={{flex:1.2, textAlign:'center', backgroundColor:'gray', color:'black', borderRadius:3, opacity:0.5}}>
                      جدیدترین های فروشگاه
                    </Text>
                    {/* <View style={{flex:1}} /> */}
                  </View>
                  <View style={{flex:6}}>
                    <Spinner 
                      visible ={this.state.spinner}
                      textContent={'لطفا صبر کنید...'}
                      textStyel={{color:'#24305E'}}
                      color='#24305E'
                    />
                    <FlatList
                              data={this.state.flat3}
                              horizontal={true}
                              // key={counter%2}
                              renderItem={({item , index})=> {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                              // console.log(Item = ${item} , Index = ${index})
                              // counter=counter+1
                              return(
                              <FlatListItem2 item = {item} index = {index} parentFlatList={this} >  

                              </FlatListItem2>);  //FlatListItem props : item , index
                              }}                    //When State changes, automatically re-render components
                          >

                    </FlatList>
                  </View>
                </View>
                <View style={{flex:8, display:'flex', flexDirection:'column'}}>
                  <GestureRecognizer
                    onSwipeLeft={()=>this.setState({modalVisible:true})}
                    onSwipeRight={()=>this.setState({modalVisible:false})}
                    config={config}
                    style={{
                      flex: 1, display:'flex', flexDirection:'column', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: 'rgb(230,230,230)' 
                    }}
                    >
                  <View style={{flex:0.8, display:'flex', flexDirection:'row', paddingTop:5}}>                 
                      <Text style={{flex:1.2, textAlign:'center', backgroundColor:'gray', color:'black', borderRadius:3, opacity:0.5}}>
                        تخفیفات ویژه
                      </Text>  
                  </View>
                  <View style={{flex:13}}>
                    <FlatList
                              data={this.state.flat}
                              horizontal={false}
                              numColumns={2}
                              // key={counter%2}
                              renderItem={({item , index})=> {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                              // console.log(Item = ${item} , Index = ${index})
                              // counter=counter+1
                              return(
                              <FlatListItem item = {item} index = {index} parentFlatList={this} >  

                              </FlatListItem>);  //FlatListItem props : item , index
                              }}                    //When State changes, automatically re-render components
                          >

                    </FlatList>
                  </View>
                  </GestureRecognizer>
                </View>
              </View>
              <Modal
                animationType="fade"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  this.setState({modalVisible: false});
                }}>
                <View style={{
                          flex: 1,
                          width:widthh,
                          justifyContent: 'flex-end',
                          alignItems: 'flex-start',
                          display:'flex',
                          flexDirection:'column'}}>
                  <View style={{flex:1, width:widthh, backgroundColor:"#F8E9A1", opacity:0}}>
                    <TouchableOpacity
                      style={{height:'100%',
                        width:'100%'}}
                      onPress={()=>this.setState({modalVisible:false})}
                    />
                  </View>
                  <View style={{
                          flex: 11,
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                          alignItems: 'flex-start',
                          display:'flex'}}>
                      <View style={{
                          flex:1,
                          height:heightt,
                          opacity:0.4,
                          backgroundColor:'black'
                        }}
                        >
                          <TouchableOpacity
                            style={{height:'100%',
                              width:'100%'}}
                            onPress={()=>this.setState({modalVisible:false})}
                          />

                      </View>
                      <View style={{
                        flex:2,
                        height: heightt,
                        backgroundColor:'white',
                        borderWidth:1,
                        borderColor:'black'}}>
                        <ScrollView>
                          <View style={{height:heightt*0.2, width:'100%'}}  >
                            <Image style={{width:1.731*widthh*0.15, height:1.264*widthh*0.15, alignSelf:'center', marginTop:heightt*0.02}} source={require('../images/LogoBlack.png')} />
                            <Text style={{textAlign:'center', marginTop:heightt*0.02 , fontSize:widthh*0.05, borderBottomWidth:2, borderBottomColor:'#24305E', color:'black', paddingBottom:5, marginRight:15, marginLeft:15}}>
                              فروشگاه
                            </Text>
                          </View>
                          <View>
                            <TouchableOpacity style={styles.taghsimBox,{borderTopWidth:0}} onPress={()=> this.setState({
                              digi:!this.state.digi, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:false, nozad:false, honar:false, })}>
                              <Text style={styles.taghsim}>
                                + کالای دیجیتال 
                              </Text>
                            </TouchableOpacity>
                            {this.state.digi &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('موبایل و تبلت و ساعت')}>
                                    <Text style={styles.joz}>
                                      موبایل و تبلت و ساعت
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('لپتاپ و کامپیوتر')}>  
                                    <Text style={styles.joz}>
                                      لپتاپ و کامپیوتر
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('عکاسی و فیلمبرداری')}>  
                                    <Text style={styles.joz}>
                                      عکاسی و فیلمبرداری
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('اداری')}>  
                                    <Text style={styles.joz}>
                                      اداری
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          </View>
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:!this.state.behdasht, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + آرایشی بهداشتی و لوازم جانبی
                            </Text>
                          </TouchableOpacity>
                          {this.state.behdasht &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('لوازم اصلاح آرایشی و بهداشتی و برقی')}>
                                    <Text style={styles.joz}>
                                      لوازم اصلاح آرایشی و بهداشتی و برقی
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('عطر و ادکلن')}>  
                                    <Text style={styles.joz}>
                                      عطر و ادکلن
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:!this.state.bagh, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + باغبانی و کشاورزی
                            </Text>
                          </TouchableOpacity>
                          {this.state.bagh &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('ابزار برقی و غیر برقی باغبانی و کشاورزی')}>
                                    <Text style={styles.joz}>
                                      ابزار برقی و غیر برقی باغبانی و کشاورزی
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:!this.state.sakhteman, mod:false, varzesh:false, khane:false, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + ابزار ساختمانی
                            </Text>
                          </TouchableOpacity>
                          {this.state.sakhteman &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('سنگ و کاشی')}>
                                    <Text style={styles.joz}>
                                      سنگ و کاشی
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('پارکت')}>  
                                    <Text style={styles.joz}>
                                      پارکت
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('رنگ')}>  
                                    <Text style={styles.joz}>
                                      رنگ
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:!this.state.mod, varzesh:false, khane:false, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + مد و پوشاک
                            </Text>
                          </TouchableOpacity>
                          {this.state.mod &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('پوشاک و اکسسوری مردانه')}>
                                    <Text style={styles.joz}>
                                      پوشاک و اکسسوری مردانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('کفش مردانه')}>  
                                    <Text style={styles.joz}>
                                      کفش مردانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('پوشاک اکسسوری زنانه')}>  
                                    <Text style={styles.joz}>
                                      پوشاک اکسسوری زنانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('کفش زنانه')}>  
                                    <Text style={styles.joz}>
                                      کفش زنانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('پوشاک بچه پسر و دختر')}>  
                                    <Text style={styles.joz}>
                                      پوشاک بچه پسر و دختر
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:!this.state.varzesh, khane:false, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + ورزشی
                            </Text>
                          </TouchableOpacity>
                          {this.state.varzesh &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('پوشاک و کفش ورزشی مردانه')}>
                                    <Text style={styles.joz}>
                                      پوشاک و کفش ورزشی مردانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('پوشاک و کفش ورزشی زنانه')}>  
                                    <Text style={styles.joz}>
                                      پوشاک و کفش ورزشی زنانه
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('لوازم جانبی ورزشی')}>  
                                    <Text style={styles.joz}>
                                      لوازم جانبی ورزشی
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:!this.state.khane, daroo:false, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + خانه و آشپزخانه
                            </Text>
                          </TouchableOpacity>
                          {this.state.khane &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('لوازم خانگی برقی و صوتی وتصویری')}>
                                    <Text style={styles.joz}>
                                      لوازم خانگی برقی و صوتی وتصویری
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('سرو و پذیرایی و آشپزخانه و دکوراتیو')}>  
                                    <Text style={styles.joz}>
                                      سرو و پذیرایی و آشپزخانه و دکوراتیو
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('ظروف پلاستیکی')}>  
                                    <Text style={styles.joz}>
                                      ظروف پلاستیکی
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('فرش')}>  
                                    <Text style={styles.joz}>
                                      فرش
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:!this.state.daroo, nozad:false, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + غذایی 
                            </Text>
                          </TouchableOpacity>
                          {this.state.daroo &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('رستوران و کافه و فستفودی')}>
                                    <Text style={styles.joz}>
                                      رستوران و کافه و فستفودی و...
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('پروتئینی')}>  
                                    <Text style={styles.joz}>
                                      پروتئینی
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('داروخانه')}>  
                                    <Text style={styles.joz}>
                                      داروخانه
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:false, nozad:!this.state.nozad, honar:false, })}>
                            <Text style={styles.taghsim}>
                              + اسباب بازی کودک ونوزاد
                            </Text>
                          </TouchableOpacity>
                          {this.state.nozad &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('اسباب بازی')}>
                                    <Text style={styles.joz}>
                                      اسباب بازی
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('لوازم کودک و نوزاد')}>  
                                    <Text style={styles.joz}>
                                      لوازم کودک و نوزاد
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          <TouchableOpacity style={styles.taghsimBox} onPress={()=> this.setState({
                              digi:false, behdasht:false, bagh:false, sakhteman:false, mod:false, varzesh:false, khane:false, daroo:false, nozad:false, honar:!this.state.honar })}>
                            <Text style={styles.taghsim}>
                              + فرهنگی و هنری
                            </Text>
                          </TouchableOpacity>
                          {this.state.honar &&
                                <View>
                                  <TouchableOpacity onPress={()=> this.daste('کتاب')}>
                                    <Text style={styles.joz}>
                                      کتاب
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('کتاب کمک درسی و لوازم التحریر')}>  
                                    <Text style={styles.joz}>
                                      کتاب کمک درسی و لوازم التحریر
                                    </Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={()=> this.daste('آلات موسیقی')}>  
                                    <Text style={styles.joz}>
                                      آلات موسیقی
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              }
                              <TouchableOpacity style={styles.taghsimBox} onPress={() => this.info()}>
                              <Text style={styles.taghsim}>
                                  درباره ما
                              </Text>
                            </TouchableOpacity>
                          <View style={{height:heightt*0.14, width:'100%'}}/>
                        </ScrollView>
                      </View>
                  </View>
                </View>
              </Modal> 
              
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible2}
                onRequestClose={() => {
                  this.setState({modalVisible2: false});
                }}>
                  <ScrollView style={{
                          width:widthh,
                          }}>
                    <TouchableOpacity style={{display:'flex', height:heightt/12, backgroundColor:'#0a1924', width: widthh, flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}} onPress={()=> this.setState({modalVisible2: false})}>
                        <Image source={require('../images/close.png')} style={{flex:1, height:heightt/50, marginLeft:20}}/>
                        <Text style={{fontSize:widthh*0.06, color:'white', textAlign:'right', flex:11, paddingRight:20}}>
                          جستجو
                        </Text>
                    </TouchableOpacity>
                    <View style={{height:(heightt*1.3)/12, backgroundColor:'gray', width: widthh, display:'flex', justifyContent:'center', alignItems:'center'}}>
                        <TextInput placeholder="دنبال چی میگردی؟" style={{width:widthh*0.9-widthh/20, marginLeft:widthh/13, borderTopRightRadius:5, borderBottomRightRadius:5, flex:1, marginTop:'3%', marginBottom:'3%', backgroundColor:'lightgray', paddingLeft:10, paddingRight:10}} onChangeText={(e)=> this.search(e)}>
                        </TextInput>
                        <View style={{height:0.075*heightt, width:heightt*0.0512, position:'absolute', top:heightt/56, left:widthh*0.024, opacity:1, backgroundColor:'lightgray', borderTopLeftRadius:5, borderBottomLeftRadius:5}}>
                        <Image source={require('../images/search.png')} style={{height:0.0512*heightt, width:heightt*0.0512, position:'absolute', top:heightt*0.0112, left:0, opacity:1, backgroundColor:'lightgray', borderTopLeftRadius:5, borderBottomLeftRadius:5}}/>
                        </View>
                    </View>
                    <View style={{height:(heightt*9.7)/12, backgroundColor:'lightgray', width: widthh, opacity:0.95}}>
                      <FlatList
                              data={this.state.flat2}
                              horizontal={false}
                              renderItem={({item , index})=> { 
                              return(
                              <View style={{width:widthh}}>
                                <TouchableOpacity  style={{display:'flex', flexDirection:"row", justifyContent:'flex-start', alignItems:'center', paddingRight:20}} onPress={(item.type == 'forooshgah')? ()=>this.go(item.name) : ()=>this.agahi(
                                  item.name, item.kind, item.imageUrl, item.gheymat, item.takhfif, item.tozih, item.forooshgah, item.jadvalName, item.jadvalTozih, item.pics) }>
                                  <Text style={{flex: 8, fontSize:widthh*0.05, color:(item.type == 'forooshgah')? 'blue': 'black', paddingRight:20, paddingBottom:heightt*0.05, paddingTop:heightt*0.05, borderBottomWidth:1, borderBottomColor:'gray', shadowColor:'black'}}>
                                    {item.name}
                                  </Text>
                                  <Image source={(item.type == 'forooshgah') ? require('../images/forooshagh.png'): require('../images/kala.png')} style={{flex:1 , height:heightt*0.06}}/>
                                </TouchableOpacity>
                              </View>);
                              }}>
                      </FlatList>
                    </View>
                  </ScrollView>
              </Modal>
        </View>
      )
  }
}

const styles = StyleSheet.create({
  taghsim:{
    fontSize:widthh*0.04,
    color:'black',
    textAlign:'right',
    padding:widthh*0.05,
    paddingTop:widthh*0.03,
    paddingBottom:widthh*0.03,
    fontWeight:'bold'
  },
  taghsimBox:{
    borderTopWidth:1,
    borderTopColor:'#374785',
  },
  joz:{
    fontSize:widthh*0.04,
    color:'dodgerblue',
    textAlign:'right',
    padding:widthh*0.05,
    paddingTop:widthh*0.02,
    paddingRight:widthh*0.08,
    paddingBottom:widthh*0.01,
    fontWeight:'bold',
  }
});

export default Home;