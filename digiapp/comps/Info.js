import React, { Fragment, Component } from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    StyleSheet,
    Dimensions,
    Linking
} from 'react-native';
import HomePic from '../images/home.png'
import About from '../images/about.png'
import Menu from '../images/menu.png'

const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);
const phoneNumber ='09113105600'
const mail = 'ali.rostami.1999@gmail.com'

export default class Info extends Component{
  static navigationOptions = {
    title:'درباره ما',
        headerStyle: {
          backgroundColor: '#0a1924',
        },
        headerTitleStyle: {
            color:'white',
            marginLeft:widthh*0.6,
            fontSize:18
        },
        headerTintColor: 'white'
  };

    finder(){
      this.props.navigation.dispatch({
        type: 'ReplaceCur',
        routeName: 'Finder',
        key: 'Finderr'
      })
    }

    render(){
      
        return(
          <View style={{ flex: 1, display:'flex', flexDirection:'column', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: 'lightorange', }}>
            <View style={{flex: 10, backgroundColor:'white', width:'100%', display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                <View style={{flex:2, marginTop:20, justifyContent:'center', alignItems:'center'}}>
                  <TouchableOpacity >  
                    <Image source={require('../images/LogoBlack.png')} style={{height:heightt*0.20, width:heightt*0.27}} />
                  </TouchableOpacity>
                </View>
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                  <TouchableOpacity onPress={()=>{Linking.openURL('https://instagram.com/arminpoulaei?igshid=a5okb2hb7yjm ')}}>
                    <Image source={require('../images/instagram.png')} style={{height:heightt*0.1, width:heightt*0.1}} />
                  </TouchableOpacity>
                </View>
                <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                  <TouchableOpacity onPress={()=>Linking.openURL(`tel:${phoneNumber}`)} style={{backgroundColor:'dodgerblue', width:widthh*0.8, borderRadius:10}}>
                    <Text style={{fontSize:22, padding:10, textAlign:'center', color:'white'}}>
                      تماس با ما
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', marginBottom:20}}>
                  <TouchableOpacity onPress={()=>Linking.openURL(`mailto:arminpoulaei@gmail.com`)} style={{backgroundColor:'dodgerblue', width:widthh*0.8, borderRadius:10}}>
                    <Text style={{fontSize:22, padding:10, textAlign:'center', color:'white'}}>
                      پست الکترونیک
                    </Text>
                  </TouchableOpacity>
                </View>
            </View>
            <View style={{flex: 1, display:'flex', flexDirection:'row', width:widthh, backgroundColor:'white'}}>
                
              </View>
          </View>
        )
    }
}