import React, { Fragment, Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  Dimensions
} from 'react-native';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';
Parse.AsyncStorage=(AsyncStorage)
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");

const heightt = Math.round(Dimensions.get('window').height);
const widthh = Math.round(Dimensions.get('window').width);

class splash extends Component {
  static navigationOptions = {
    header: null,
    title: null
  };
  constructor(props) {
    super(props);
    this.state = {
      timing: 0,
    };
    setInterval(() => (
      this.setState(previousState => (
        { timing: previousState.isShowingText + 10 }
      ))
    ), 2000);
  }


  render() {
    if (this.state.timing < 5000) {
      return (
        <View style={{ flex: 1, width:widthh ,justifyContent: 'center', alignItems: 'center', backgroundColor: 'dodgerblue' }}>
            <Image style={{width:1.731*widthh*0.4, height:1.264*widthh*0.4}} source={require('../images/LogoWhite.png')}/>
        </View>
      )
    } else {
      this.props.navigation.dispatch({
        type: 'ReplaceCur',
        routeName: 'Home',
        key: 'Homee'
      })
      return (null)
    }
  }
}

export default splash;