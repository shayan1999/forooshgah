/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './navi';
import { name as appName } from './app.json';
import Parse from 'parse/react-native'
import { AsyncStorage } from 'react-native';


Parse.setAsyncStorage(AsyncStorage);
Parse.serverURL = 'http://185.162.235.140:1337/parse';
Parse.initialize("KLOUDBOY123");

AppRegistry.registerComponent(appName, () => App);