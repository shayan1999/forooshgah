import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import Splash from './comps/splash'
import Home from './comps/Home'
import Finder from './comps/Finder'
import Info from './comps/Info'
import Agahi from './comps/Agahi'
import Forooshgah from './comps/Forooshgah'

const MainNavigator = createStackNavigator({
    Splash: {
        screen: Splash
    },
    Home: {
        screen: Home
    },
    Finder:{
        screen: Finder
    },
    Info:{
        screen: Info
    },
    Agahi:{
        screen: Agahi
    },
    Forooshgah:{
        screen: Forooshgah
    }

});

const PrevGetStackForAction = MainNavigator.router.getStateForAction;
MainNavigator.router = {
    ...MainNavigator.router,
    getStateForAction(action, state) {
        if (state && action.type === 'ReplaceCur') {
            const routes = state.routes.slice(0, state.routes.length - 1);
            routes.push(action);
            return {
                ...state,
                routes,
                index: routes.length - 1,
            }
        }
        return PrevGetStackForAction(action, state);
    }
}

const App = createAppContainer(MainNavigator);

export default App;